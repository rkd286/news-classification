import numpy as np
import pandas as pd
from gensim.utils import simple_preprocess
import spacy

from collections import Counter

class PreprocessData:
    '''
    Creates a tokenized object of the input documents.

    docs: a pd.Series  object.
    '''

    def __init__(self, docs):
        self.docs = docs
        self.tokens = []
        content_array = self.docs.values
        tokens = []
        print("Tokenizing the documents...")
        for sentence in content_array:
            self.tokens.append(simple_preprocess(sentence, deacc=True))
        print("Done.")
    

    def word_count(self):
        
        '''
        Returns the mean, min and max word count of each document.
        '''

        word_count_dict = {}
        lengths = np.array([len(sent) for sent in self.tokens])
        word_count_dict['words_mean'] = lengths.mean()
        word_count_dict['words_max'] = lengths.max()
        word_count_dict['words_min'] = lengths.min()
        return word_count_dict

    def lemmatizer(self, allowed_postags = ['NOUN', 'VERB', 'ADJ', 'ADV']):
        
        '''
        Returns a lemmatized list of documents.
        '''

        self.lemmatized = []
        nlp = spacy.load('en', disable = ['ner', 'parser'])
        print('Lemmatizing...')
        for text in self.tokens:
            document = nlp(" ".join(text))
            self.lemmatized.append(" ".join([token.lemma_ if token.lemma_ not in ['-PRON-'] else '' for token in document 
                if token.pos_ in allowed_postags]))
        print('Done.')
        return self.lemmatized



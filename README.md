# News Classification

## Objective
The goal is to apply topic modeling on the dataset to discover abstract news categories.

# Dataset
**Source** - [Kaggle - All the news Dataset](https://www.kaggle.com/snapcrack/all-the-news)

The publications include the New York Times, Breitbart, CNN, Business Insider, the Atlantic, Fox News, Talking Points Memo, Buzzfeed News, National Review, New York Post, the Guardian, NPR, Reuters, Vox, and the Washington Post.
